<?php
namespace App\Services;

use App\Repositories\BookRepository;
use App\Repositories\MovieRepository;
use Illuminate\Support\Facades\DB;
use Exception;
use Illuminate\Auth\Events\Validated;
use Illuminate\Support\Facades\Validator;
use InvalidArgumentException;

class BookingService
{
    protected $bookRepository;


    public function __construct(BookRepository $bookRepository)
    {
        $this->bookRepository = $bookRepository;
    }

    public function bookTicket($data)
    {  //decrement available seats

        $result = $this->bookRepository->bookTicket($data);
        return $result;
    }

    public function getTickets($id)
    {
        return $this->bookRepository->tickets($id);
    }

    public function cancelTicket($b_id, $seats_booked, $m_id)
    {
        DB::beginTransaction();
        $this->bookRepository->cancelBooking($b_id, $seats_booked, $m_id);
        DB::commit();
        return true;
    }

    public function purchaseTickets($id)
    {
        $result_booking = $this->bookRepository->purchasedTickets($id);
        return $result_booking;
    }
}