<?php
namespace App\Services;

use App\Repositories\BookRepository;
use App\Repositories\MovieRepository;
use Illuminate\Http\Request;
use Exception;
use Illuminate\Auth\Events\Validated;

use InvalidArgumentException;

class MovieService
{
    protected $bookRepository;
    protected $movieRepository;


    public function __construct(BookRepository $bookRepository, MovieRepository $movieRepository)
    {
        $this->bookRepository = $bookRepository;
        $this->movieRepository = $movieRepository;
    }

    public function viewAdmin(){
        return $this->movieRepository->viewMovies();
    }

    public function addMovie($movieData){


        $result = $this->movieRepository->addAMovie($movieData);
        return $result;
    }

    public function viewMovie(){
        return $this->movieRepository->viewMovies();
    }

    public function movieOption(){
        return $this->movieRepository->movieOptions();
    }

    public function ajaxComplete($id){
        return $this->movieRepository->ajaxComplete($id);
    }

    public function editMovie($movieData){
            $id = $movieData['id'];
            $result = $this->movieRepository->editMovie($movieData,$id);
            return $result;
    }

    public function deleteMovie($movieData){
        $id = $movieData['id'];
        $result = $this->movieRepository->deleteMovie($id);
        return $result;
    }

    public function getMail(){
        return $this->movieRepository->getMail();
    }

}