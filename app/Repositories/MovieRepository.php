<?php

namespace App\Repositories;

use App\Models\Movie;
use App\Models\Booking;
use Carbon\Carbon;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;

class MovieRepository

{

    protected $movie;


    public function __construct(Booking $ticket, Movie $movie){
        $this->ticket = $ticket;
        $this->movie = $movie;
    }

    public function addAMovie($movieData){
        $movie = new $this->movie;
        $movie ->title=  $movieData['movie_title'];
        $movie ->movie_description =  $movieData['movie_description'];
        $movie ->available_seats =  $movieData['seat_number'];
        $movie ->show_time =  $movieData['show_time'];
        $movie ->created_at =  Carbon::now();
        $movie ->updated_at =  Carbon::now();
        $movie->save();
        return $movie->fresh();
    }

    public function viewMovies(){

        $userTickets = $this->ticket->rightJoin('movie','booking.movie_id','=','movie.id')
        ->select('movie.title','movie.available_seats')
        ->selectRaw('sum(ifnull(booking.seats_booked,0)) AS total_seats_booked')
        ->groupBy('movie.id')->get();
        return $userTickets;
    }


    public function movieOptions(){ 
        return $this->movie->get();
    }

    public function ajaxComplete($id){
        return $this->movie->where('id',$id)->get();
            // $data = DB::table('movie')->where('id',$id)->get();
    }

    public function editMovie($movieData,$id){
        $movie = $this->movie->find($id);
        $movie->title = $movieData['movie_title'];
        $movie->movie_description = $movieData['movie_description'];
        $movie->show_time = $movieData['show_time'];
        $movie->available_seats = $movieData['seat_number'];
        $movie->update();
        return $movie;
    }

    public function deleteMovie($id){
        $movie = $this->movie->find($id);
        $seatsBooked = $this->ticket->where('movie_id',$movie->id)->selectRaw('ifnull(sum(seats_booked),0) AS total_seats_booked')->first();
        if($movie->show_time > Carbon::now() || $seatsBooked->total_seats_booked != 0){
            Session::flash('alert', "Uh Oh, seems like you can't delete this movie");
            return back();
        }
        $movie->delete();

        return $movie;
    }

    public function getMail(){
        $details = $this->ticket->rightJoin('movie','booking.movie_id','=','movie.id')
        ->select('movie.title','movie.available_seats','movie.show_time','movie.id')
        ->selectRaw('sum(ifnull(booking.seats_booked,0)) AS total_seats_booked')
        ->groupBy('movie.id')->get();
        return $details;
    }
}
