<?php

namespace App\Repositories;

use App\Models\Booking;
use App\Models\Movie;

class BookRepository

{

    protected $ticket;


    public function __construct(Booking $ticket, Movie $movie){
        $this->ticket = $ticket;
        $this->movie = $movie;
    }

    public function tickets($id){ 
        $result = $this->movie->find($id);
        return $result;
    }

    public function bookTicket($data){ 
        $ticket = new $this->ticket;
        $ticket ->movie_id =  $data['movieid'];
        $ticket ->user_id =  $data['userid'];
        $ticket ->seats_booked =  $data['seatsbooked'];
        $ticket->save();

        $movie = $this->movie->find($data['movieid']);
        $movie->available_seats -= $data['seatsbooked'];
        $movie->update();

        return [$ticket->fresh(),$movie->fresh()];
    }


    public function purchasedTickets($id){
        

        $userTickets = $this->ticket->join('movie','booking.movie_id','=','movie.id')->where('user_id',$id)->select('booking.seats_booked','booking.movie_id','booking.user_id','booking.created_at','booking.id','movie.title')->get();
        return $userTickets;
    }


    public function cancelBooking($b_id,$seats_booked,$m_id){
        $movie = $this->movie->find($m_id);
        $movie->available_seats += $seats_booked;
        $movie->update();
        $ticket = $this->ticket->find($b_id);
        $ticket->delete();
        
        return true;
    }
}