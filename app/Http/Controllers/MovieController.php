<?php

namespace App\Http\Controllers;

use App\Mail\MovieBooking;
use Illuminate\Http\Request;

use App\Models\Movie;
use App\Rules\ShowDate;
use App\Services\MovieService;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class MovieController extends Controller
{
        protected $movieService;

        public function __construct(MovieService $movieService){
            $this->movieService = $movieService;
        }

    public function dashboard(){
        if(Auth::user()->is_admin){
            $movies = $this->movieService->viewAdmin(); 
            return view('admin',['movies'=>$movies]);
        }else{
            $movies = Movie::get();
            // echo $movies->title;
            return view('dashboard', ['movies'=>$movies]);
        }   
    }

    public function addMovie(Request $request){
        $movieData = $request->input();
        $validator = Validator::make($movieData, [
            'movie_title'=>'required',
            'movie_description'=>'required',
            'show_time'=>['required',new ShowDate],
            'seat_number'=>'required|gte:0|integer'
        ],[
            'movie_title.required'=>'Please enter the title for the movie',
            'movie_description.required'=>'Please enter the description for the movie',
            'show_time.required'=>'Please enter the show time for the movie',
            'seat_number.required'=>'Please enter the number of seats available',
        ]);
        if ($validator->fails()) {
            return back()->withErrors($validator,'book');
        }
        $this->movieService->addMovie($movieData);
        return redirect('dashboard')->with(["status"=>"Movie Has Been Added"]);
    }

    public function viewMovie(){
        $movies = $this->movieService->viewMovie();
        return view('viewmovie',['tickets'=>$movies]);
    }

    public function movieOptionEdit(){

        $options = $this->movieService->movieOption();
        return view('editmovie',['movieOptions'=>$options]);
    }
    public function ajaxComplete($id){
        $data = $this->movieService->ajaxComplete($id);
        return response()->json(['info'=>$data]);
    }

    
    public function editMovie(Request $request){
        $movieData = $request->input();
        if($request->has('delete')){
            $this->movieService->deleteMovie($movieData);
            return redirect('dashboard')->with(["status"=>"Movie Has Been Deleted"]);

        }

        $validator = Validator::make($movieData, [
            'movie_title'=>'required',
            'movie_description'=>'required',
            'show_time'=>['required',new ShowDate],
            'seat_number'=>'required|gte:0|integer',
        ],[
            'movie_title.required'=>'Please enter the title for the movie',
            'movie_description.required'=>'Please enter the description for the movie',
            'show_time.required'=>'Please enter the show time for the movie',
            'seat_number.required'=>'Please enter the number of seats available',
        ]);
        if ($validator->fails()) {
            return back()->withErrors($validator,'book');
        }

        if($request->has('edit')){
            $this->movieService->editMovie($movieData);
            return redirect('dashboard')->with(["status"=>"Movie Has Been Edited"]);

        }


    }

    public function mail(){
        $mailBody = $this->movieService->getMail();
        $details = [
            'title' => 'Ticket Booking Report',
            'body' => $mailBody
        ];
        Mail::to(Auth::user()->email)->send(new MovieBooking($details));
        return redirect('/');

    }


        
    

}
