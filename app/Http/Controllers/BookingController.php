<?php

namespace App\Http\Controllers;

use App\Mail\MovieBooking;
use App\Mail\TIcketBooking;
use App\Models\Booking;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Auth\Events\Registered;
use App\Services\BookingService;
use App\Models\Movie;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Providers\RouteServiceProvider;



class BookingController extends Controller
{
    protected $bookingService;

    public function __construct(BookingService $bookingService){
        $this->bookingService = $bookingService;
    }

    public function getTickets(Request $request){
        $id = $request->id;
        $result = $this->bookingService->getTickets($id);
        return view('book', ['moviess'=>$result]);
    }

    public function bookTickets(Request $request){

        $data = $request->input();
        
        $validator = Validator::make($data, [
            'movieid'=>'required',
            'userid'=>'required',
            'seatsbooked'=>'required'
        ],[
            'seatsbooked.required'=>'Uh Oh! Please enter seat number'
        ]);
        if ($validator->fails()) {
            return back()->withErrors($validator,'book');
            // return redirect('book')->withErrors($validator,'book');
        }

        $result['data'] = $this->bookingService->bookTicket($data);
        $mailBody = $result['data'];
        $details = [
            'title' => 'Welcome To The Cinema',
            'body' => $mailBody
        ];
        Mail::to('anil.niranjana.01@gmail.com')->send(new TicketBooking($details));
        return redirect('dashboard')->with(["book"=>"You booked a show! Enjoy!"]);
    }

    public function getUserTickets(){
        $uid = Auth::user()->id;
        $userTickets = $this->bookingService->purchaseTickets($uid);
        return view('cancel',['userHistory'=>$userTickets]);
    }

    public function purchaseHistory(){

        $uid = Auth::user()->id;
        $userTickets = $this->bookingService->purchaseTickets($uid);
        return view('purchase',['userHistory'=>$userTickets]);
    }

    public function cancelTickets(Request $request){
        $bookingId = $request->booking_id;
        $movieId = $request->movie_id;
        $seatsBooked = $request->seats_booked;
        $this->bookingService->cancelTicket($bookingId,$seatsBooked,$movieId);
        return redirect('dashboard')->with(["cancel"=>"You cancelled your booking"]);

    }
    
}
