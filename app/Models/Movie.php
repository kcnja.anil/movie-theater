<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Movie extends Model
{
    use HasFactory;
    protected $table = 'movie';

    protected $fillable = [
        'title',
        'movie_description',
        'available_seats',
        'show_time'
    ];

    public function Booking(){
        return $this->morphMany(Booking::class,'booking');
    }

    public function getShowDateAttribute(){
        return date('Y-M-d',strtotime($this->attributes['show_time']));
    }

    public function getShowAttribute(){
        return date('H-i',strtotime($this->attributes['show_time']));
    }

}
