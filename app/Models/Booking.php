<?php

namespace App\Models;

use GuzzleHttp\Psr7\Request;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Booking extends Model
{
    use HasFactory;
    protected $table = 'booking';

    protected $fillable = [
        'movie_id',
        'user_id',
        'seats_booked',
    ];

    public function booking()
    {
        return $this->morphTo();
    }

    public function getBookingDateAttribute(){
        return date('d-M-Y',strtotime($this->attributes['created_at']));
    }


}
