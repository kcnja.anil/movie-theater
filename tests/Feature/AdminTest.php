<?php

namespace Tests\Feature;

use App\Models\User;
use Carbon\Carbon;
use Carbon\Factory;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Request;
use Tests\TestCase;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class AdminTest extends TestCase
{
    use RefreshDatabase;
    
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_example()
    {

        $this->assertTrue(true);
    }

    public function test_admin_screen_can_be_rendered()
    {
        $user = User::factory()->create(['is_admin' => 1]);
        $response = $this->actingAs($user)->get('/dashboard');

        $response->assertStatus(200);
    }

    public function test_editmovie_screen_can_be_rendered()
    {
        $user = User::factory()->create(['is_admin' => 1]);
        $response = $this->actingAs($user)->get('/dashboard/editmovie');

        $response->assertStatus(200);
    }

    public function test_addmovie_screen_can_be_rendered()
    {
        $user = User::factory()->create(['is_admin' => 1]);
        $response = $this->actingAs($user)->get('/dashboard/addmovie');

        $response->assertStatus(200);
    }

    public function test_viewmovie_screen_can_be_rendered()
    {
        $user = User::factory()->create(['is_admin' => 1]);
        $response = $this->actingAs($user)->get('/dashboard/viewmovie');

        $response->assertStatus(200);
    }


    public function test_create_movie_without_middleware(){ // create movie without any authentication
        $data = [
            'title' => 'QWERTY',
            'movie_description' => 'this is qwerty',
            'available_seats' => "40",
            'show_time' => Carbon::tomorrow(),
        ];
        // Auth::logout();

        $response = $this->post('dashboard/addmovie',$data);
        $response->assertStatus(302);
        $response->assertRedirect('/login');
    }


    public function test_create_movie(){
        $data = [
            'movie_title' => "XYZ",
            'movie_description' => "xyz xyz xyz xyz",
            'show_time' => '2022-04-19T12:35',
            'seat_number' => "30",
    
        ];
        $user = User::factory()->create(['is_admin' => 1]);

        $this->actingAs($user)
            ->post('dashboard/addmovie',$data)
            ->assertStatus(302)
            ->assertRedirect('/dashboard')
            ->assertSessionHas("status",'Movie Has Been Added');


    }

    public function test_admin_can_update_movie(){ //only successful when a row already exists
        $data = [
            'movie_title' => "XYZ",
            'movie_description' => "xyz xyz xyz xyz",
            'show_time' => '2022-04-19T12:35',
            'seat_number' => "30",
    
        ];
        $user = User::factory()->create(['is_admin' => 1]);

        $this->actingAs($user)
            ->post('dashboard/addmovie',$data)
            ->assertStatus(302)
            ->assertRedirect('/dashboard')
            ->assertSessionHas("status",'Movie Has Been Added');


        $data = [
            'id' => '2',
            'movie_title' => "edited-movie",
            'movie_description' => "edited description",
            'show_time' => '2021-05-19T12:35',
            'seat_number' => "35",
            'edit' => 'Edit Movie'
    
        ];
        $this->actingAs($user)->post('dashboard/editmovie',$data)
                              ->assertStatus(302)
                              ->assertRedirect('dashboard')
                              ->assertSessionHas("status",'Movie Has Been Edited');
    }

    public function test_admin_can_delete_movie(){ //only successful when a row already exists

        $data = [
            'movie_title' => "XYZ",
            'movie_description' => "xyz xyz xyz xyz",
            'show_time' => '2022-04-19T12:35',
            'seat_number' => "30",
    
        ];
        $user = User::factory()->create(['is_admin' => 1]);

        $this->actingAs($user)
            ->post('dashboard/addmovie',$data)
            ->assertStatus(302)
            ->assertRedirect('/dashboard')
            ->assertSessionHas("status",'Movie Has Been Added');


        $data = [
            'id' => "3",
            'movie_title' => "XYZ",
            'movie_description' => "xyz xyz xyz xyz",
            'show_time' => '2021-04-19T12:35',
            'seat_number' => "30",
            'delete' => 'Delete Movie'
    
        ];
        $this->actingAs($user)->post('dashboard/editmovie',$data)
                              ->assertStatus(302)
                              ->assertRedirect('dashboard')
                              ->assertSessionHas("alert","Uh Oh, seems like you can't delete this movie");
                             

    }
}
