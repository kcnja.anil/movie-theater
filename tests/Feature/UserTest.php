<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use App\Models\User;
use Tests\TestCase;

class UserTest extends TestCase
{
    use RefreshDatabase;
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_example()
    {
        $response = $this->get('/');

        $response->assertStatus(200);
    }

    public function test_user_can_access_dashboard_without_middleware(){
        $this->get('dashboard')
             ->assertStatus(302)
             ->assertRedirect('/login');
    }

    public function test_user_can_render_dashboard(){
        $user = User::factory()->create(['is_admin' => 0]);
        $this->actingAs($user)
             ->get('dashboard')
             ->assertStatus(200);

    }

    public function test_user_can_render_cancel(){
        $user = User::factory()->create(['is_admin' => 0]);
        $this->actingAs($user)
             ->get('dashboard/cancel')
             ->assertStatus(200);
    }

    public function test_user_can_render_purchase(){
        $user = User::factory()->create(['is_admin' => 0]);
        $this->actingAs($user)
             ->get('dashboard/purchase')
             ->assertStatus(200);
    }

    public function test_user_can_render_book(){
        $data = [
            'movie_title' => "XYZ",
            'movie_description' => "xyz xyz xyz xyz",
            'show_time' => '2022-04-19T12:35',
            'seat_number' => "30",
    
        ];
        $admin = User::factory()->create(['is_admin' => 1]);

        $this->actingAs($admin)
            ->post('dashboard/addmovie',$data)
            ->assertStatus(302)
            ->assertRedirect('/dashboard')
            ->assertSessionHas("status",'Movie Has Been Added');

        $id = '4';
        $user = User::factory()->create(['is_admin' => 0]);
        $this->actingAs($user)
             ->get('dashboard/book?id='.$id)
             ->assertStatus(200);
    }

    public function test_user_can_book_ticket(){
        $data = [
            'movie_title' => "XYZ",
            'movie_description' => "xyz xyz xyz xyz",
            'show_time' => '2022-04-19T12:35',
            'seat_number' => "30",
    
        ];
        $admin = User::factory()->create(['is_admin' => 1]);

        $this->actingAs($admin)
            ->post('dashboard/addmovie',$data)
            ->assertStatus(302)
            ->assertRedirect('/dashboard')
            ->assertSessionHas("status",'Movie Has Been Added');

        $data = [
            'movieid'=>'5',
            'userid'=> $admin->id,
            'seatsbooked'=>'5'
        ];
        $user =  User::factory()->create((['is_admin' => 0]));
        $this->actingAs($user)
             ->post('dashboard/book',$data)
             ->assertStatus(302)
             ->assertRedirect('dashboard')
             ->assertSessionHas('book','You booked a show! Enjoy!');

    }

    public function test_user_can_cancel_ticket(){
        $data = [
            'movie_title' => "XYZ",
            'movie_description' => "xyz xyz xyz xyz",
            'show_time' => '2022-04-19T12:35',
            'seat_number' => "30",
    
        ];
        $admin = User::factory()->create(['is_admin' => 1]);

        $this->actingAs($admin)
            ->post('dashboard/addmovie',$data)
            ->assertStatus(302)
            ->assertRedirect('/dashboard')
            ->assertSessionHas("status",'Movie Has Been Added');

        $user =  User::factory()->create((['is_admin' => 0]));

        $data = [
            'movieid'=>'6',
            'userid'=> $user->id,
            'seatsbooked'=>'5'
        ];
        
        $this->actingAs($user)
                ->post('dashboard/book',$data)
                ->assertStatus(302)
                ->assertRedirect('dashboard')
                ->assertSessionHas('book','You booked a show! Enjoy!');

        $data = [
            'movie_id'=>'6',
            'booking_id'=>'2',
            'seats_booked'=>'5'
        ];
        $this->actingAs($user)
             ->post('dashboard/cancel',$data)
             ->assertStatus(302)
             ->assertRedirect('dashboard')
             ->assertSessionHas('cancel','You cancelled your booking');
    }
}
