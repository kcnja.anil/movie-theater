<?php

use App\Http\Controllers\MovieController;
// use App\Http\Controllers\admin\MovieController;
use App\Http\Controllers\admin\ViewMovie;
use App\Http\Controllers\BookingController;
use App\Http\Controllers\ViewController;
use App\Models\Movie;
use App\Http\Controllers\BookingControllertrial;
use Database\Seeders\BookingSeeder;
use Illuminate\Routing\RouteGroup;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::Get('dashboard',[MovieController::class,'dashboard'])->name('dashboard')->middleware(['auth']);


require __DIR__.'/auth.php';


Route::middleware(['auth','is_user'])->group(function () {
    Route::get('/dashboard/book', [BookingController::class,'getTickets'])->name('book');
    Route::post('/dashboard/book', [BookingController::class,'bookTickets'])->name('dashboard/cancel');
    Route::get('/dashboard/cancel',[BookingController::class,'getUserTickets'])->name('cancel');

    Route::post('/dashboard/cancel',[BookingController::class,'cancelTickets'])->name('dashboard/cancel');
    Route::get('/dashboard/purchase',[BookingController::class,'purchaseHistory'])->name('purchase');   
});

Route::middleware(['auth', 'is_admin'])->group(function () {
    Route::get('dashboard/addmovie', function () {
        return view('addmovie');
    })->name('addmovie');
    Route::post('dashboard/addmovie', [MovieController::class,'addMovie']);
    Route::get('dashboard/viewmovie', [MovieController::class,'viewMovie'])->name('viewmovie');
    Route::get('dashboard/editmovie', [MovieController::class,'movieOptionEdit'])->name('editmovie');
    Route::get('dashboard/editmovie/{id}', [MovieController::class,'ajaxComplete']);
    Route::post('dashboard/editmovie', [MovieController::class,'editMovie']);
    Route::get('dashboard/mail',[MovieController::class,'mail']);
});



