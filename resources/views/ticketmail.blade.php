<h3 class="aligncenter" style="font-family: 'Helvetica Neue',Helvetica,Arial,'Lucida Grande',sans-serif; box-sizing: border-box; font-size: 20px; color: #000; line-height: 1.2em; font-weight: 400; text-align: center; margin: 40px 0 0;" align="center">{{ $details['title'] }}</h3>
<div style="font-size: 15px">
<p>Hello Dear Customer,</p><br>
<p>You have succesfully booked a show! The details of your ticket are provided below</p><br>
<p><b>Movie : </b>{{$details['body'][1]->title}}</p><br>
<p><b>Show Time : </b>{{$details['body'][1]->show_time}}</p><br>
<p><b> Seats Booked : </b>{{$details['body'][0]->seats_booked}}</p><br>
<br>
<hr>
<p>We hope you you have a great time<br> Thank you,<br>The Cinema</p>
</div>