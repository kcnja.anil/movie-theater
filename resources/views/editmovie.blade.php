
<x-app-layout>
    <x-slot name="header">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Edit/Delete Movie') }}
        </h2>
    </x-slot>
    @if(Session::has('errors'))
    <div class="pl-4 border-red-600 bg-red-200 ">
        Whoops! You missed some fields.
    </div>
    @endif
    <div class="py-8">
        <div class="flex items-center w-full ">
            <div class="w-full bg-green-100 rounded shadow-lg p-8 m-3 md:max-w-md md:mx-auto">
                <div class="block w-full  text-grey-darkest mb-6">
                    <form class="mb-4"  method="post" action="{{url('/dashboard/editmovie')}}" id="editform">
                        {{csrf_field()}}
                        <div class="flex flex-col mb-4">
                            <label class="mb-2 uppercase font-bold text-lg text-grey-darkest"  for="movie_options">Movie Options</label>
                            <select class="border py-2 px-3 text-grey-darkest" name="movieoptions" id="movie_options">
                                <option disabled selected>select movie</option>
                                @foreach($movieOptions as $movie)
                                    <option value="{{$movie->id}}">{{$movie->id}}.{{$movie->title}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="flex flex-col mb-4">               
                            <label class="mb-2 uppercase font-bold text-lg text-grey-darkest"  for="movie_title">Movie title: </label>
                            <input class="border py-2 px-3 text-grey-darkest" type="text" name="movie_title" id="movie_title">
                            <span class="text-sm text-red-500">{{$errors->book->first('movie_title')}}</span>

                        </div>
                        <div class="flex flex-col mb-4">
                            <label class="mb-2 uppercase font-bold text-lg text-grey-darkest"  for="movie_description">Description</label>
                            <textarea class="border py-2 px-3 text-grey-darkest h-24" type="text" name="movie_description" id="movie_description"></textarea>
                            <span class="text-sm text-red-500">{{$errors->book->first('movie_description')}}</span>

                            </div>
                        <div class="flex flex-col mb-4">                    
                            <label class="mb-2 uppercase font-bold text-lg text-grey-darkest"  for="id">ID: </label>
                            <input class="border py-2 px-3 text-grey-darkest" type="text" readonly name="id" id="id">
                        </div>
                        <div class="flex flex-col mb-4">                    
                            <label class="mb-2 uppercase font-bold text-lg text-grey-darkest"  for="show_time">Show Time</label>
                            <input class="border py-2 px-3 text-grey-darkest" type="datetime-local" name="show_time" id="show_time">
                            <span class="text-sm text-red-500">{{$errors->book->first('show_time')}}</span>
                        </div>
                        <div class="flex flex-col mb-4">                    
                            <label class="mb-2 uppercase font-bold text-lg text-grey-darkest"  for="seat_number">Number of Seats</label>
                            <input class="border py-2 px-3 text-grey-darkest" type="number" name="seat_number" id="seat_number" >
                            <span class="text-sm text-red-500">{{$errors->book->first('seat_number')}}</span>

                        </div>
                        <div class=" m-auto w-max">                    
                            <input  class=" transition duration-700 ease-in-out bg-green-200 hover:bg-blue-300  transform hover:-translate-y-1 hover:scale-110 p-2 mt-5 mr-4 uppercase mx-auto rounded text-gray-700" type="submit" value="Edit Movie" name="edit">
                            <input  class=" transition duration-700 ease-in-out bg-green-200 hover:bg-blue-300  transform hover:-translate-y-1 hover:scale-110 p-2 mt-5 uppercase mx-auto rounded text-gray-700" type="submit" value="Delete Movie" name="delete">
                        </div>
                    </form>
                    
                </div>
            </div>
        </div>
    </div>
<script>
    $("#movie_options").change(function() {
        var title = document.getElementById('movie_title');
        var description = document.getElementById('movie_description');
        var id = document.getElementById('id');
        var showTime = document.getElementById('show_time');
        var seatNumber = document.getElementById('seat_number');
$.ajax({
    url: 'editmovie/' + $(this).val(),
    type: 'get',
    data: {},
    success: function(data) {
            title.value = data.info[0]["title"];
            description.value = data.info[0]["movie_description"];
            id.value = data.info[0]["id"];
            showTime.value = data.info[0]["show_time"].split(" ").join('T');
            seatNumber.value = data.info[0]["available_seats"];
            console.log(data.info[0]["show_time"].split(" ").join(''))
        

    },
    error: function(jqXHR, textStatus, errorThrown) {}
});
});
</script>
</x-app-layout> 