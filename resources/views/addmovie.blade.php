<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Add Movie') }}
        </h2>
    </x-slot>

    @if(Session::has('errors'))
    <div class="pl-4 border-red-600 bg-red-200 ">
        Whoops! Please Check your fields.
    </div>
    @endif
    <div class="py-4 ">
        <div class="flex items-center w-full ">
            <div class="w-full bg-green-100 rounded shadow-lg p-8 m-14 md:max-w-md md:mx-auto">
                <div class="block w-full  text-grey-darkest mb-6">
                    <form class="mb-4" method="post" action="{{url('/dashboard/addmovie')}}">
                        {{csrf_field()}}
                        <div class="flex flex-col mb-4">
                            <label class="mb-2 uppercase font-bold text-lg text-grey-darkest" for="movie_title">Movie Title</label>
                            <input class="border py-2 px-3 text-grey-darkest" type="text"  name="movie_title" id="movie_title">
                            <span class="text-sm text-red-500">{{$errors->book->first('movie_title')}}</span>
                        </div>
                        <div class="flex flex-col mb-4">
                            <label class="mb-2 uppercase font-bold text-lg text-grey-darkest" for="movie_description">Description</label>
                            <input class="border py-2 px-3 text-grey-darkest" type="text" name="movie_description" id="movie_description">
                            <span class="text-sm text-red-500">{{$errors->book->first('movie_description')}}</span>
                        </div>
                        <div class="flex flex-col mb-4">
                            <label class="mb-2 uppercase font-bold text-lg text-grey-darkest" for="show_time">Show Time</label>
                            <input class="border py-2 px-3 text-grey-darkest" type="datetime-local" name="show_time" id="show_time">
                            <span class="text-sm text-red-500">{{$errors->book->first('show_time')}}</span>
                        </div>
                        <div class="flex flex-col mb-4">
                            <label class="mb-2 uppercase font-bold text-lg text-grey-darkest" for="seat_number">Number of Seats</label>
                            <input class="border py-2 px-3 text-grey-darkest" type="number" name="seat_number" id="seat_number">
                            <span class="text-sm text-red-500">{{$errors->book->first('seat_number')}}</span>
                        </div>
                        <div>
                            <input  class="block transition duration-700 ease-in-out bg-green-200 hover:bg-blue-300  transform hover:-translate-y-1 hover:scale-110 p-2 mt-5 uppercase mx-auto rounded text-gray-700" type="submit" value="Add Movie">
                        </div>
                    </form>
                    
                </div>
            </div>
        </div>
</x-app-layout>