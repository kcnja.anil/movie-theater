<x-app-layout>
    <x-slot name="header">
        <div class="flex">
            <x-dashboard-logo class="block h-10 w-auto fill-current" />
            <h2 class="font-semibold text-xl mt-2 ml-3 text-gray-800 leading-tight">
                {{ __('Dashboard') }}
            </h2>
        </div>
    </x-slot>
        {{-- <div class="border-red-600 bg-red-200">
            {{$errors->book->first()}}
        </div> --}}
   
        <div class="pl-4 bg-green-200 border-green-800">
            {{Session::get('book') ?? ''}}
        </div>
        <div class="pl-4 bg-yellow-200 border-yellow-500">
            {{Session::get('cancel') ?? ''}}
        </div>

    

    <div class="py-5">
        <div class=" shadow-lg max-w-7xl mx-auto sm:px-6 lg:px-8 p-10">
            <div class="grid grid-cols-2 gap-5">
                @foreach($movies as $movie)
                            <div class="w-full h-full bg-white rounded-md shadow-lg  px-2 py-4 ">
                                <form class="movies" method="get" action="{{url('dashboard/book')}}">
                                    <div class="">
                                        <div class="block text-center py-4 m-auto uppercase">
                                            <b> {{$movie->title}}</b> 
                                        </div> 
                                        <div> <b>About The Movie</b>
                                            <div class="movie-description">
                                                {{$movie->movie_description}}
                                            </div>   
                                        </div>
                                        <div>
                                            <div class="my-5"><b>Show time</b> 
                                                <div class="">{{$movie->show_date}}</div>
                                                <div>{{$movie->show}}</div>
                                            </div>
                                            @if ($movie->available_seats==0)
                                                <button type="submit" class="block mx-auto mt-5 bg-gray-400 text-white rounded p-2 uppercase" name="id" value="{{$movie->id}}" disabled>Sold OUT !</button>
                                            @else
                                                <button type="submit" class=" block transition duration-700 ease-in-out bg-green-200 hover:bg-blue-300 transform hover:-translate-y-1 hover:scale-110 p-2 mt-5 uppercase mx-auto rounded text-gray-700" name="id" value="{{$movie->id}}">Book This Show</button> 
                                                <div class="text-xs pt-3 text-right text-red-600">
                                                    @if($movie->available_seats<10)
                                                        Hurry, Only a few more seats left!
                                                    @endif
                                                </div>
                                            @endif

                                        </div> 
                                    </div>
                                </form>
                            </div>
            
                @endforeach
            </div>
        </div>
    </div>
    
</x-app-layout>

