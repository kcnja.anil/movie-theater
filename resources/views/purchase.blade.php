<x-app-layout>
    <x-slot name="header">
        <div class="flex">
            <x-purchase-logo class="block h-10 w-auto fill-current" />
            <h2 class=" font-semibold text-xl mt-2 ml-3 text-gray-800 leading-tight">
                {{ __('My Purchases') }}
            </h2>
        </div>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="  overflow-hidden shadow-lg">
                <div class="p-6  ">
                    <table class="w-full shadow-lg bg-white">
                        <thead>
                            <tr class="h-16">
                                <th class="bg-green-100 border text-xl">Movie</th>
                                <th class="bg-green-100 border text-xl">Seats Booked</th>
                                <th class="bg-green-100 border text-xl">Booked On</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($userHistory as $item)
                                <tr>
                                    <td class="border text-lg">{{$item->title}}</td>
                                    <td class="border text-center">{{$item->seats_booked}}</td>
                                    <td class="border text-center">{{$item->booking_date}}</td>
                                </tr>
                        
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
