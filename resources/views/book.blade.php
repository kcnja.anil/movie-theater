<x-app-layout>
    <x-slot name="header">
        <div class="flex">
            <x-ticket-logo class="block h-10 w-auto fill-current" />
            <h2 class=" font-semibold text-xl mt-2 ml-3 text-gray-800 leading-tight">
                {{ __('Movie Booking') }}
            </h2>
        </div>
    </x-slot>
    @if(Session::has('errors'))
    <div class="border-red-600 bg-red-200 ">
        {{$errors->book->first()}}
    </div>
    @endif

 
    <div class="py-8">
        <div class="flex items-center max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="overflow-hidden shadow-sm sm:rounded-lg md:max-w-md md:mx-auto">
                <div class="p-6 border-b border-gray-200">
                    <form class="mb-4 p-10 bg-white shadow-lg " method="post" action="{{url('dashboard/book')}}">
                        {{csrf_field()}}
                        <input type="text" value="{{$moviess->id}}" name="movieid" hidden>
                        <input type="text" value="{{Auth::user()->id}}"name="userid" hidden>
                    <div class="mb-2 uppercase font-bold text-lg text-grey-darkest text-center">{{$moviess->title}}</div>
                    <div class="grid grid-row-1 gap-5">
                        <div class="grid grid-cols-2 gap-2 mb-4">                    
                            <label class="mb-2 uppercase font-bold text-md text-grey-darker" for="seats-available">Available Seats</label>
                            <input class="border py-2 px-3 text-grey-darkest" type="text" value="{{$moviess->available_seats}}" readonly>
                        </div>

                        <div class="grid grid-cols-2 mb-4">
                            <label class="mb-2 uppercase font-bold text-md text-grey-darker" for="seatsbooked">Seats:</label>
                            <input class="border py-2 px-3 text-grey-darkest" type="number" max="{{$moviess->available_seats}}" placeholder="How many seats?" min=1 name="seatsbooked">
                        </div>
                    </div>
                    <div>
                    <input class=" block transition duration-700 ease-in-out bg-green-200 hover:bg-blue-300  transform hover:-translate-y-1 hover:scale-101 text-gray-700 uppercase text-md mx-auto p-4 rounded mt-5" type="submit" name="submit-booking" value="Book My Ticket!" id=""> 
                    </div>
                    </form>
                    
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
