<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Dashboard') }}
        </h2>
    </x-slot>

    @if(Session::has('alert'))
        <div class="pl-4 bg-red-300">
            {{Session::get('alert')}}
        </div>
    @else
        <div class="pl-4 bg-indigo-300">
            {{Session::get('status')}}
        </div>
    @endif
    <div class="pt-12 ">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8 mb-10">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg mb-10">
                <div class="p-6 bg-white border-b border-gray-200">
                    You are an Admin.
                    <div class="text-right text-lg text-green-700">
                        <a href="{{url('dashboard/mail')}}">Get Report</a>
                    </div>
                </div>
            </div>

            <div class=" overflow-hidden shadow-sm sm:rounded-lg ">
                <div class="grid grid-cols-2 gap-5">
                    @foreach($movies as $movie)
                        <div class="w-full h-full bg-white rounded-md shadow-lg  px-2 py-4 ">
                            <div class="block text-center py-4 m-auto uppercase">
                                <b> {{$movie->title}}</b> 
                            </div> 
                            <div> {{round(($movie->total_seats_booked / ($movie->total_seats_booked+$movie->available_seats))*100)}}% Booking
                                <div class="h-3 relative max-w-xl rounded-full overflow-hidden">
                                    <div class="absolute bg-gray-200 w-full h-full"> </div>
                                        <div class="absolute bg-gradient-to-r bg-green-200 hover:bg-blue-300  h-full" style="width:{{round(($movie->total_seats_booked / ($movie->total_seats_booked+$movie->available_seats))*100)}}%"></div>
                                   
                                </div>
                            </div>

                        </div>
                
                    @endforeach
                </div>
            </div>

        </div> 
    </div>
</x-app-layout>