<h3 class="aligncenter" style="font-family: 'Helvetica Neue',Helvetica,Arial,'Lucida Grande',sans-serif; box-sizing: border-box; font-size: 20px; color: #000; line-height: 1.2em; font-weight: 400; text-align: center; margin: 40px 0 0;" align="center">{{ $details['title'] }}</h3>

<table >
    <thead>
        <tr >
            <th style="padding: 3px">Movie ID</th>
            <th style="padding: 3px">Movie Title</th>
            <th style="padding: 3px">Show Time</th>
            <th style="padding: 3px">Seats Booked</th>
            <th style="padding: 3px">Available Seats</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($details['body'] as $item)
            <tr>
                <td align="center" style="padding: 3px">{{$item->id}}</td>
                <td align="center" style="padding: 3px">{{$item->title}}</td>
                <td align="center" style="padding: 3px">{{$item->show_time}}</td>
                <td align="center" style="padding: 3px">{{$item->total_seats_booked}}</td>
                <td align="center" style="padding: 3px">{{$item->available_seats}}</td>
            </tr>
    
        @endforeach
    </tbody>
</table>

<p>Thank You</p>