<x-app-layout>
    <x-slot name="header">
        <div class="flex">
            <x-cancel-logo class="block h-10 w-auto fill-current" />
            <h2 class="font-semibold text-xl mt-2 ml-3 text-gray-800 leading-tight">
                {{ __('Cancel') }}
            </h2>
        </div>

    </x-slot>

    <div class="py-5 ">
        <div class=" items-center">
            <div class="w-full rounded shadow-lg p-8 m-4 md:max-w-6xl md:mx-auto">
                <div class="grid grid-cols-3 gap-4 ">
                        @foreach ($userHistory as $item)
                            <div class="shadow-lg my-5 bg-gray-100">
                                <form  method="post" action="{{url('dashboard/cancel')}}">
                                    {{csrf_field()}}
                                    <div class="pl-5">
                                        <div class="my-2 text-center uppercase font-bold text-lg text-grey-darkest">{{$item->title}}</div>
                                        <input type="text" name="movie_id" value="{{$item->movie_id}}" hidden>
                                        <div class="text-center ">
                                            <label for="seatsbooked">Seats Booked:</label>
                                            <input class="bg-gray-100 text-center" id="seatsbooked" name="seats_booked" value="{{$item->seats_booked}}">
                                        </div>
                                        <div><button class=" block transition duration-700 ease-in-out bg-green-200 hover:bg-blue-300  transform hover:-translate-y-1 hover:scale-110 text-gray-700 p-2 mt-5 uppercase mx-auto rounded" type="submit" name="booking_id" value="{{$item->id}}">Cancel</button></div><br><br>
                                    </div>
                                </form>
                            </div>
                        @endforeach
                        
                </div>
            </div>
        </div>
    </div>
</x-app-layout>