<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('View Movie') }}
        </h2>
    </x-slot>
    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="overflow-hidden shadow-lg sm:rounded-lg">
                <div class="p-6 ">

                    <table class="w-full bg-white shadow-md">
                        <thead>
                            <tr class="h-16">
                                <th class="bg-green-100 border text-xl">Movie</th>
                                <th class="bg-green-100 border text-xl">Tickets Booked</th>
                                <th class="bg-green-100 border text-xl">Seats Available</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($tickets as $item)
                                <tr>
                                    <td class="border text-lg">{{$item->title}}</td>
                                    <td class="border text-center">{{$item->total_seats_booked}}</td>
                                    <td class="border text-center">{{$item->available_seats}}</td>
                                </tr>
                            
                            @endforeach
                        </tbody>
                </table>
            </div>
        </div>
    </div>
</x-app-layout>